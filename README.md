# Spoilerinator.

Tool to make spoilered images (+ content warnings) easier to create and compatible(ish) across Matrix clients.

# Usage.
- Upload image in an *unencrypted* room.
- Secondary (left) click on it.
- Copy image URL.

![Secondary click menu, "copy image address".](img/copy-image-address.png)
- Launch in terminal.
```sh
python3 spoilerinator.py
```
- Paste the URL.
- (Optional) Provide a content notice.

# Thanks.
- [Jade](https://github.com/Jadefalke256) for help with regex (`((.[^\/]*){2})$`).
- [Æthena](https://codeberg.org/Tzeentch) for name suggestion.
- [Sasha](https://sashanoraa.gay) for helping with git and testing.
- [Polychromata](https://animalfound.family/@polychromata) for pointing out Element mobile's preview despite `<blockquote>`s.

# Design.
The spoilenator attempts to balance usability and compatibility across the frustratingly widely inconsistent implementations of spoilered images, biggest offenders being Element mobile and Nheko, which simply do not spoiler images under any circumstances, others that scale inline images differently from each other if no `height` and/or `width` attribute (which only accept pixels) is given, Element mobile which is the only client to reender a link preview despite the `<blockquote>` element and FluffyChat which will scale a URL without content to a part of the following element (not to be confused with Element, a client that makes it easy to web search for how it handles HTML elements).

# Examples in client rendering.
## Element/SchildiChat.

Side note: it's recommended to check out [SchildiChat](https://schildi.chat) if you like Element.

![](img/spoiler-element-covered.png)
![](img/spoiler-element-uncovered.png)

![](img/spoiler-schildichat-covered.png)
![](img/spoiler-schildichat-uncovered.png)

## Element/SchildiChat mobile.

<img src="img/spoiler-element-mobile-uncovered-vertical.png" width="300">
<img src="img/spoiler-element-mobile-uncovered-horizontal.png" width="300">

## FluffyChat.
![](img/spoiler-fluffychat-covered.png)
![](img/spoiler-fluffychat-uncovered.png)

## Nheko.
![](img/spoiler-nheko-uncovered.png)

## Cinny (ignore the weird text color).
![](img/spoiler-cinny-covered.png)
![](img/spoiler-cinny-uncovered.png)

# TODO:
- Capitalize only the first letter of CN.
- URL validity check.
- Work with `mxc://` URLs.
- Add easy way to switch to CW and TW.