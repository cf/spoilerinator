import re

mxImgURL = input('Matrix image URL (not message hyperlink): ').strip()
if not mxImgURL:
	mxImgURL = 'https://matrix-client.matrix.org/_matrix/media/r0/download/matrix.org/UlBbPLUmdeLmjzRPZYCjgjrw' # Sets an example if empty for testing purposes.
mxImgMXC = 'mxc://' + re.compile(r'[^\/]+\/[^\/]+\/?$').search(mxImgURL).group()# Takes out the two last URL segments and prefixes 'mxc://'.

imgPreviewDeferral = '<a href="https://codeberg.org/crimsonfork/CN"></a>'		# First URL in sequence blocks the other preview(s) on L-ement mobile.
imgHeight = '69'																# Keeps picture small on clients that don't spoiler images (L-ement mobile, Nheko).

cNColor = '#dc143c'																# Color of the CN part.
cNText = input('Content Notice: ')
cNTextEnd = ''
contentNotice = f'<font color="{cNColor}" data-mx-color="{cNColor}"><b>Spoiler.</b></font><br>'
if cNText:																		# Adds a CN if one was provided and appends a dot.
	if not cNText.endswith('.'):
		cNTextEnd = '.'
	contentNotice = f'<font color="{cNColor}" data-mx-color="{cNColor}"><b>CN</b></font>: <a href="{mxImgURL}">{cNText}</a>{cNTextEnd}<br>'

spoilerImgHTML = f'/html <blockquote>{imgPreviewDeferral}{contentNotice}<span data-mx-spoiler=""><a href="{mxImgURL}"><img src="{mxImgMXC}" height="{imgHeight}"></a></span></blockquote>'

print(spoilerImgHTML)